const fetch = require('node-fetch');

const requestJson = {
	"companyName": "Hallo.io",
	"email": "admin@hallo.io",
	"howToApply": "email me",
	"jobDescription": "**Software Engineer**\n<a href='javascript:alert(1)'>xss</a>\n[xss link](javascript:alert(1))\n[safe link](https://hallo.io)",
	"location": "Singapore",
	"jobTitle": "Software Engineer",
	"url": "https://hallo.io"
};

fetch('https://71dht6x2pb.execute-api.us-east-1.amazonaws.com/dev/jobs/create', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(requestJson, null, 4)
}).then(function(res) {
  return res.json();
}).then((json) => {
  console.log(`Response: ${JSON.stringify(json)}`);
}).catch(() => {
  console.log("POST request failed!");
});

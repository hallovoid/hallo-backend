const handlebars = require('handlebars');
const fs = require('fs');
const Promise = require('bluebird');

var data = {
  canonicalLink: 'https://google.com',
  jobTitle: '</h1><a href="javascript:alert(1)">Software Engineer</a><h1>',
  datePosted: '2016-10-10',
  jobDescription: '<a href="javascript:alert(1)">XSS</a><b>Hello, this is the job description</b>',
  location: 'Singapore',
  companyName: 'Hallo',
  companyLogo: { url: 'https://d3d5dhvngr2uu3.cloudfront.net/img/BkEI1rlNl.png', width: 200, height: 95},
  url: 'https://google.com'
}

var readFile = Promise.promisify(fs.readFile);
var writeFile = Promise.promisify(fs.writeFile);

readFile('handlebars-amp.html', 'utf-8')
  .then(function(source) {
    var template = handlebars.compile(source);
    var html = template(data);
    console.log(html);
    return html;
  }).then(function(html) {
    return writeFile("./output.html", html);
  }).catch(function(error) {
    console.log("Error reading file", error);
  });

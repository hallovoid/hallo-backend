const handlebars = require('handlebars');
const fs = require('fs');
const Promise = require('bluebird');

var data = {
  jobTitle: 'Software Engineer',
  location: 'location',
  companyName: 'Hallo'
}
data.body = process.argv[2];

var readFile = Promise.promisify(require("fs").readFile);

readFile('handlebars-example.html', 'utf-8')
  .then(function(source) {
    var template = handlebars.compile(source);
    var html = template(data);
    console.log(html)
  }).catch(function(error) {
    console.log("Error reading file", error);
  });

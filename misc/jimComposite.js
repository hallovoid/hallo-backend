var Jimp = require("jimp");

Jimp.read("input.jpg").then(function(image) {
  var backgroundImage = new Jimp(480, 258, 0xFFFFFFFF);
  // this image is 480 x 258, every pixel is set to 0x00000000
  var x = (480 - 280) / 2;
  var y = (258 - 150) / 2;
  backgroundImage.composite(image, x, y)
       .write("output.jpg");
}).catch(function (err) {
  console.error(err);
});

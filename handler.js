'use strict';

const Job = require('./src/job');
const job = new Job();

module.exports.create = (event, context, callback) => {
  job.create(JSON.parse(event.body), callback);
};

module.exports.preview = (event, context, callback) => {
  job.preview(JSON.parse(event.body), callback);
};

module.exports.resize = (event, context, callback) => {
  job.resize(JSON.parse(event.body), callback);
};

module.exports.hello = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
    }),
  };

  callback(null, response);

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
};

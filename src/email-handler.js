'use strict';

const AWS = require('aws-sdk');
const Promise = require('bluebird');

AWS.config.setPromisesDependency(Promise);

const ses = new AWS.SES({apiVersion: '2010-12-01', region: 'us-east-1'});

class EmaiHandler {
  sendJobCreationEmail(email, url) {
		const params = {
			Source: process.env.WEB_FROM_EMAIL,
			Destination: {
				ToAddresses: [email]
			},
			Message: {
				Subject: {
					Data: 'Job Posting has been created'
				},
				Body: {
					Text: {
						Data: `Your job posting url: ${url}`
					}
				}
			}
		};

    if (!process.env.WEB_SEND_JOB_CREATION_EMAIL) {
      return Promise.resolve(null);
    }

    return ses.sendEmail(params).promise();
  }
}

module.exports = EmaiHandler;

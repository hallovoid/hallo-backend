'use strict';

const Promise = require('bluebird');
const Jimp = require('jimp');
const probe = require('probe-image-size');
var env = require('env-var');

class ImageProcessor {
  processCompanyLogo(companyLogoBuffer) {
    const companyLogoMaxWidth = env('WEB_COMPANY_LOGO_MAX_WIDTH').required().asPositiveInt();
    if (!shallResizeCompanyLogo(companyLogoBuffer)) {
      return Promise.resolve(companyLogoBuffer);
    }

    function jimpReadCompanyLogoBuffer() {
      return Jimp.read(companyLogoBuffer);
    }

    function jimpResizeCompanyLogo(image) {
      image.resize(companyLogoMaxWidth, Jimp.AUTO);
      var getBufferAsync = Promise.promisify(image.getBuffer, {context: image});
      return getBufferAsync(Jimp.MIME_PNG);
    }

    function shallResizeCompanyLogo(buf) {
      return probe.sync(buf).width > companyLogoMaxWidth;
    }

    return Promise.try(jimpReadCompanyLogoBuffer)
                  .then(jimpResizeCompanyLogo);
  }

  resizeCompanyLogo(companyLogoBuffer) {
    const companyLogoMaxWidth = 480; //env('WEB_COMPANY_LOGO_MAX_WIDTH').required().asPositiveInt();
    const newImageWidth = 512;
    const newImageHeight = 256;

    function jimpReadCompanyLogoBuffer() {
      return Jimp.read(companyLogoBuffer);
    }

    function jimpResizeCompanyLogo(image) {
      if (!shallResizeCompanyLogo(image)) {
        return Promise.resolve(image);
      }
      image.scaleToFit(0.9 * newImageWidth, 0.9 * newImageHeight);
      return image;
    }

    function jimpCompositeCompanyLogo(image) {
      var backgroundImage = new Jimp(newImageWidth, newImageHeight, 0xFFFFFFFF);
      var x = (newImageWidth - image.bitmap.width) / 2;
      var y = (newImageHeight - image.bitmap.height) / 2;
      // Place the image at the center of the background image.
      backgroundImage.composite(image, x, y);
      return backgroundImage;
    }

    function getImageBuffer(image) {
      var getBufferAsync = Promise.promisify(image.getBuffer, {context: image});
      return getBufferAsync(Jimp.MIME_PNG);
    }

    function shallResizeCompanyLogo(image) {
      return (image.bitmap.width > 0.9 * newImageWidth) || (image.bitmap.height > 0.9 * newImageHeight);
    }

    return Promise.try(jimpReadCompanyLogoBuffer)
                  .then(jimpResizeCompanyLogo)
                  .then(jimpCompositeCompanyLogo)
                  .then(getImageBuffer);
  }
}

module.exports = ImageProcessor;

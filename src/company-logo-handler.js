'use strict';

const Promise = require('bluebird');
const S3Utils = require('./s3-utils');
const s3Utils = new S3Utils();
const ImageProcessor = require('./image-processor');
const imageProcessor = new ImageProcessor();

class CompanyLogoHandler {
  constructor() {
  }

  convertBase64EncodedImageToBuffer(base64EncodedImage) {
    return new Buffer(base64EncodedImage.replace(/^data:image\/\w+;base64,/, ""),'base64');
  }

  execute(domainName, companyLogoBase64) {
    if (!companyLogoBase64) {
      return null;
    }

    const companyLogoBuffer = this.convertBase64EncodedImageToBuffer(companyLogoBase64);
    let savedCompanyLogoObject = {};

    function processCompanyLogo() {
      return imageProcessor.processCompanyLogo(companyLogoBuffer);
    }

    function uploadCompanyLogoS3(imgBuffer) {
      return s3Utils.uploadCompanyLogoS3(domainName, imgBuffer);
    }

    function getCompanyLogoObject(companyLogoObject) {
      savedCompanyLogoObject["thumbnail"] = companyLogoObject;
      return null;
    }

    function getCompanyLogoForOpenGraphImageTag(companyLogoObject) {
      savedCompanyLogoObject["openGraphImageTag"] = companyLogoObject;
      return savedCompanyLogoObject;
    }

    function resizeCompanyLogoForOpenGraphImageTag() {
      return imageProcessor.resizeCompanyLogo(companyLogoBuffer);
    }

    return Promise.try(processCompanyLogo)
                  .then(uploadCompanyLogoS3)
                  .then(getCompanyLogoObject)
                  .then(resizeCompanyLogoForOpenGraphImageTag)
                  .then(uploadCompanyLogoS3)
                  .then(getCompanyLogoForOpenGraphImageTag);
  }

  resize(domainName, companyLogoBase64) {
    if (!companyLogoBase64) {
      return null;
    }

    const companyLogoBuffer = this.convertBase64EncodedImageToBuffer(companyLogoBase64);

    function processCompanyLogo() {
      return imageProcessor.resizeCompanyLogo(companyLogoBuffer);
    }

    function uploadCompanyLogoS3(imgBuffer) {
      console.log("Upload company logo to S3");
      return s3Utils.uploadCompanyLogoS3(domainName, imgBuffer);
    }

    return Promise.try(processCompanyLogo)
                  .then(uploadCompanyLogoS3);
  }
}

module.exports = CompanyLogoHandler;


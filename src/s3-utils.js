'use strict';

const shortid = require('shortid');
const dateFormat = require('dateformat');
const probe = require('probe-image-size');
const slug = require('limax');
const AWS = require('aws-sdk');
const Promise = require('bluebird');

AWS.config.setPromisesDependency(Promise);

const s3 = new AWS.S3();

class S3Utils {
  generateCompanyLogoS3Key() {
    return `${process.env.AWS_IMAGE_S3_FOLDER}/${shortid.generate()}.png`;
  }

  generateCompanyLogoUrl(domainName, companyLogoS3Key) {
    if (!domainName) {
			var params = {Bucket: process.env.AWS_AMP_PREVIEW_S3_BUCKET, Key: companyLogoS3Key, Expires: 900};
			return s3.getSignedUrl('getObject', params);
    }
    return `${domainName}/${companyLogoS3Key}`;
  }

  generateAmpHtmlS3Key(inputJson, todayDate) {
    const dateString = dateFormat(todayDate, "yyyymmdd");
    const hourMinuteString = dateFormat(todayDate, "HHMMss");
    const companyName = inputJson.companyName;
    const jobTitle = inputJson.jobTitle;
    var filename = slug(`${hourMinuteString} ${companyName} ${jobTitle}`);
    return `${dateString}/${filename}.html`;
  }

  generateS3Params(s3Key, contentType, content, domainName) {
    const isPreview = domainName == null;
    return {
      Bucket: isPreview ? process.env.AWS_AMP_PREVIEW_S3_BUCKET : process.env.AWS_AMP_S3_BUCKET,
      Key: s3Key,
      ContentType: contentType,
      Body: content
    };
  }

  generateAmpHtmlUrl(domainName, s3Key) {
    if (!domainName) {
			var params = {Bucket: process.env.AWS_AMP_PREVIEW_S3_BUCKET, Key: s3Key, Expires: 900};
			return s3.getSignedUrl('getObject', params);
    }
    return `${domainName}/${s3Key}`;
  }

  uploadCompanyLogoS3(domainName, imgBuffer) {
    const companyLogoS3Key = this.generateCompanyLogoS3Key();
    const params = this.generateS3Params(companyLogoS3Key, 'image/png', imgBuffer, domainName);
    const imageProbeResult = probe.sync(imgBuffer);
    const companyLogoUrl = this.generateCompanyLogoUrl(domainName, companyLogoS3Key);
    const companyLogoObject = {
      url: companyLogoUrl,
      width: imageProbeResult.width,
      height: imageProbeResult.height,
    };

    function uploadFileToS3() {
      console.log("uploadFileToS3: ", params.Key);
      return s3.putObject(params).promise();
    }

    function returnCompanyLogoObject() {
      return Promise.resolve(companyLogoObject);
    }

    return Promise.try(uploadFileToS3)
                  .then(returnCompanyLogoObject)
  }

  uploadAmpHtmlToS3(s3Key, ampHtml, domainName) {
    const params = this.generateS3Params(s3Key, 'text/html', ampHtml, domainName);
    return s3.putObject(params).promise();
  }
}

module.exports = S3Utils;

'use strict';

class HttpResponseGenerator {
  execute(json) {
    return {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : "*" // Required for CORS support to work
      },
      body: JSON.stringify(json)
    };
  }
}

module.exports = HttpResponseGenerator;

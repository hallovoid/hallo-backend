'use strict';

const Promise = require('bluebird');
const stripe = require('stripe')(process.env.STRIPE_TOKEN);

class PaymentHandler {
  pay(paymentToken) {
    function sendRequestToStripe() {
      return stripe.charges.create({
				amount: 1000,
				currency: "usd",
				description: "Job Posting for hallo.io",
				source: paymentToken,
			});
    }

    return Promise.try(sendRequestToStripe);
  }
}

module.exports = PaymentHandler;

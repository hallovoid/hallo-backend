'use strict';

const Promise = require('bluebird');
const S3Utils = require('./s3-utils');
const s3Utils = new S3Utils();
const AmpUtils = require('./amp-utils');
const ampUtils = new AmpUtils();

class AmpHandler {
  execute(inputJson, domainName, companyLogoObject) {
    const now = new Date();
    const s3Key = s3Utils.generateAmpHtmlS3Key(inputJson, now);
    const templateView = ampUtils.generateAmpHtmlTemplateView(s3Key, inputJson, companyLogoObject);

    function generateAmpHtml() {
      return ampUtils.generateAmpHtml(templateView);
    }

    function uploadAmpHtmlToS3(ampHtml) {
      return s3Utils.uploadAmpHtmlToS3(s3Key, ampHtml, domainName);
    }

    function generateAmpHtmlUrl() {
      const url = s3Utils.generateAmpHtmlUrl(domainName, s3Key);
      return url;
    }

    return Promise.try(generateAmpHtml)
                  .then(uploadAmpHtmlToS3)
                  .then(generateAmpHtmlUrl);
  }
}

module.exports = AmpHandler;

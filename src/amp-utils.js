'use strict';

const dateFormat = require('dateformat');
const Remarkable = require('remarkable');
const md = new Remarkable();
var removeMd = require('remove-markdown');
const now = new Date();
const handlebars = require('handlebars');
const fs = require("fs");
const Promise = require('bluebird');
const readFile = Promise.promisify(fs.readFile);
const path = require('path');

handlebars.registerHelper("stripMarkdown", function(input, length) {
  var output = removeMd(input);
  // Escape double quotes as it is used in json+ld.
  // Get rid of all '\n' and extra spaces..
  let rawString = output.replace(/"/g, '\\"').replace(/[\n\r]+/g, '').replace(/\s{2,10}/g, ' ');

  // If the length is not specified, just simply return the raw string.
  if (!length || rawString.length <= length) {
    return rawString;
  }

  return rawString.substring(0, length) + "...";
});

handlebars.registerHelper("markdown2Html", function(input) {
  return md.render(input);
});

md.set({
  breaks: true, // Convert '\n' in paragraphs into <br>
  linkify: true, // Autoconvert URL-like text to links
});

class AmpUtils {
  generateAmpHtml(templateView) {
    const templateFilePath = path.join(__dirname, '..', process.env.WEB_AMP_TEMPLATE);
    const cssFilePath = path.join(__dirname, '..', process.env.WEB_CSS_FILE);

    function readAmpTemplateFile() {
      return readFile(templateFilePath, 'utf-8');
    }

    function readCssFile(source) {
      return readFile(cssFilePath, 'utf-8');
    }

    function fillTemplate(content) {
      var template = handlebars.compile(content.html);
      templateView.css = content.css;
      var html = template(templateView);
      return html;
    }

    return Promise.join(readAmpTemplateFile(), readCssFile(), function(htmlTemplateContent, cssContent) {
                    return {
                      "html": htmlTemplateContent,
                      "css": cssContent
                    };
                  })
                  .then(fillTemplate);
  }

  generateAmpHtmlTemplateView(s3Key, inputJson, companyLogoObject) {
    function replaceWithEmptyStringIfAllWhiteSpaces(input) {
      if (!input || !input.replace(/[\u200B-\u200D\uFEFF\s+]/g, '').length) {
        return "";
      }
      return input;
    }

    function preprocessInputJson(inputJson) {
      inputJson.responsibilities = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.responsibilities);
      inputJson.skills = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.skills);
      inputJson.educationRequirements = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.educationRequirements);
      inputJson.experienceRequirements = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.experienceRequirements);
      inputJson.qualifications = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.qualifications);
      inputJson.incentiveCompensation = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.incentiveCompensation);
      inputJson.jobBenefits = replaceWithEmptyStringIfAllWhiteSpaces(inputJson.jobBenefits);
    }

    function showAdditionalDescription(inputJson) {
      return (inputJson.responsibilities
          || inputJson.skills
          || inputJson.educationRequirements
          || inputJson.experienceRequirements
          || inputJson.qualifications
          || inputJson.incentiveCompensation
          || inputJson.jobBenefits) ? true : false;
    }

    preprocessInputJson(inputJson);

    return {
      "canonicalLink": s3Key.substr(s3Key.indexOf("/") + 1),
      "jobTitle": inputJson.jobTitle,
      "jobDescription": inputJson.jobDescription,
      "howToApply": inputJson.howToApply,
      "jobLocation": inputJson.location,
      "companyName": inputJson.companyName,
      "companyLogo": companyLogoObject,
      "url": inputJson.url,
      "employmentType": inputJson.employmentType,
      "workHours": inputJson.workHours,
      "salary": inputJson.salary,
      "responsibilities": inputJson.responsibilities,
      "skills": inputJson.skills,
      "educationRequirements": inputJson.educationRequirements,
      "experienceRequirements": inputJson.experienceRequirements,
      "qualifications": inputJson.qualifications,
      "incentiveCompensation": inputJson.incentiveCompensation,
      "jobBenefits": inputJson.jobBenefits,
      "showAdditionalDescription": showAdditionalDescription(inputJson),
      "datePosted": dateFormat(now, "yyyy-mm-dd"),
      "datePostedLongFormat": dateFormat(now, "d mmm yyyy")
    };
  }
}

module.exports = AmpUtils;

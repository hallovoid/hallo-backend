'use strict';

const Promise = require('bluebird');
const CompanyLogoHandler = require('./company-logo-handler');
const companyLogoHandler = new CompanyLogoHandler();
const AmpHandler = require('./amp-handler');
const ampHandler = new AmpHandler();
const PaymentHandler = require('./payment-handler');
const paymentHandler = new PaymentHandler();
const EmailHandler = require('./email-handler');
const emailHandler = new EmailHandler();
const HttpResponseGenerator = require('./http-response-generator');
const httpResponseGenerator = new HttpResponseGenerator();

const domainName = process.env.WEB_DOMAIN;

class Job {
  create(inputJson, callback) {
    var generatedAmpUrl = "";
    function handleCompanyLogo() {
      return companyLogoHandler.execute(domainName, inputJson.companyLogo);
    }

    function handleAmp(companyLogoObject) {
      return ampHandler.execute(inputJson, domainName, companyLogoObject);
    }

    function handleAmpUploadSuccessEvent(url) {
      generatedAmpUrl = url;
      return Promise.resolve(null);
    }

    function handlePayment() {
      if (!process.env.STRIPE_ENABLED) {
        return Promise.resolve(null);
      }
      return paymentHandler.pay(inputJson.paymentToken);
    }

    function sendEmail() {
      return emailHandler.sendJobCreationEmail(inputJson.email, generatedAmpUrl);
    }

    function handleSuccess() {
      const json = generateJson(true, generatedAmpUrl);
      callback(null, httpResponseGenerator.execute(json));
    }

    function handleFailure(err) {
      console.log(err.stack);
      const json = generateJson(false, null);
      callback(null, httpResponseGenerator.execute(json));
    }

    return Promise.try(handleCompanyLogo)
           .then(handleAmp)
           .then(handleAmpUploadSuccessEvent)
           .then(handlePayment)
           .then(function(charge) {
             console.log(JSON.stringify(charge));
           })
           .then(sendEmail)
           .then(handleSuccess)
           .catch(handleFailure);
  }

  preview(inputJson, callback) {
    var generatedAmpUrl = "";
    function handleCompanyLogo() {
      return companyLogoHandler.execute(null, inputJson.companyLogo);
    }

    function handleAmp(companyLogoObject) {
      return ampHandler.execute(inputJson, null, companyLogoObject);
    }

    function handleAmpUploadSuccessEvent(url) {
      generatedAmpUrl = url;
      return Promise.resolve(null);
    }

    function handleSuccess() {
      const json = generateJson(true, generatedAmpUrl);
      callback(null, httpResponseGenerator.execute(json));
    }

    function handleFailure(err) {
      console.log(err.stack);
      const json = generateJson(false, null);
      callback(null, httpResponseGenerator.execute(json));
    }

    return Promise.try(handleCompanyLogo)
           .then(handleAmp)
           .then(handleAmpUploadSuccessEvent)
           .then(handleSuccess)
           .catch(handleFailure);
  }

  resize(inputJson, callback) {
    var generatedAmpUrl = "";
    function handleCompanyLogo() {
      return companyLogoHandler.resize(null, inputJson.companyLogo);
    }

    function handleSuccess() {
      const json = generateJson(true, generatedAmpUrl);
      callback(null, httpResponseGenerator.execute(json));
    }

    function handleFailure(err) {
      console.log(err.stack);
      const json = generateJson(false, null);
      callback(null, httpResponseGenerator.execute(json));
    }

    return Promise.try(handleCompanyLogo)
           .then(handleSuccess)
           .catch(handleFailure);
  }
}

function generateJson(success, url) {
  var result = {
    success: success
  };

  if (url) {
    result.url = url;
  }

  return result;
}

module.exports = Job;

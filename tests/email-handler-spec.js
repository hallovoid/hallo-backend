const chai = require('chai');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const Promise = require('bluebird');
var config = require('../config');

// Constants
const email = config.web.from_email;
const url = "dummy url";
const params = {
	Source: email,
	Destination: {
		ToAddresses: [email]
	},
	Message: {
		Subject: {
			Data: 'Job Posting has been created'
		},
		Body: {
			Text: {
				Data: `Your job posting url: ${url}`
			}
		}
	}
};

// Stubs
const fakeS3Promise = function() { return Promise.resolve(); };
const sendEmailStub = sinon.stub().returns({ promise: fakeS3Promise });

const EmailHandler = proxyquire('../src/email-handler', {
  'aws-sdk': {
    'SES': function () {
      return {
       sendEmail: sendEmailStub
      };
    }
  }
});
const emailHandler = new EmailHandler();

describe('EmailHandler', function() {
  afterEach(function() {
    sendEmailStub.reset();
  });

  it('sendJobCreationEmail() should send email to the user who created the job post', function() {
    const result = emailHandler.sendJobCreationEmail(email, url);
    return result.then(function() {
      sendEmailStub.should.have.been.calledOnce.calledWith(params);
    });
  });

  it('sendJobCreationEmail() should not send email if send_job_creation_email in the config is set to false', function() {
    config.web.send_job_creation_email = false;
    const result = emailHandler.sendJobCreationEmail(email, url);
    return result.then(function() {
      sendEmailStub.should.have.not.been.called;
    });
  });
});

const chai = require('chai');
const chaiAsPromised = require("chai-as-promised");
const proxyquire = require('proxyquire');
const sinonChai = require("sinon-chai");
const sinon = require('sinon');
require('sinon-as-promised');
chai.use(sinonChai);

chai.use(chaiAsPromised);
chai.should();

// Constants
const domainName = "https://example.com";
const dummyTemplateView = "dummy templateView";
const dummyUrl = "dummy url";
const dummyS3Key = "dummy s3 key";
const dummyAmp = "dummy amp";

// Stubs
const generateAmpHtmlTemplateViewStub = sinon.stub().returns(dummyTemplateView);
const generateAmpHtmlStub = sinon.stub().returns(dummyAmp);
const uploadAmpHtmlToS3Stub = sinon.stub();
const generateAmpHtmlS3KeyStub = sinon.stub().returns(dummyS3Key);
const generateAmpHtmlUrlStub = sinon.stub().returns(dummyUrl);

const AmpHandler = proxyquire('../src/amp-handler', {
  './s3-utils': function() {
    return {
      'generateAmpHtmlS3Key': generateAmpHtmlS3KeyStub,
      'uploadAmpHtmlToS3': uploadAmpHtmlToS3Stub,
      'generateAmpHtmlUrl': generateAmpHtmlUrlStub
    };
  },
  './amp-utils': function() {
    return {
      'generateAmpHtml': generateAmpHtmlStub,
      'generateAmpHtmlTemplateView': generateAmpHtmlTemplateViewStub
    };
  }
});
const ampHandler = new AmpHandler();

describe('AmpHandler', function() {
  it('execute() should generate AMP, upload to S3 and then return the URL', function() {
    const result = ampHandler.execute({}, domainName, {});

    generateAmpHtmlS3KeyStub.should.have.been.calledOnce;
    generateAmpHtmlTemplateViewStub.should.have.been.calledOnce;

    return result.then(function(url) {
      generateAmpHtmlStub.should.have.been.calledOnce.calledWith(dummyTemplateView);
      uploadAmpHtmlToS3Stub.should.have.been.calledOnce.calledWith(dummyS3Key, dummyAmp);
      generateAmpHtmlUrlStub.should.have.been.calledOnce.calledWith(domainName, dummyS3Key);
      url.should.equal(dummyUrl);
    });
  });
});

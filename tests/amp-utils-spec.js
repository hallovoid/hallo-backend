const chai = require('chai');
const expect = chai.expect;
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const dateFormat = require('dateformat');

// Constants
const templateView = {"dummyKey": "dummyValue"};
const dummyHtmlContent = "<html>dummy html content</html>";
const s3Key = "somepath/somerandomkey.html";
const inputJson = {
  "jobTitle": "Software Engineer",
  "jobDescription": "*Some job description*",
  "howToApply": "Send email to someone",
  "location": "Singapore",
  "companyName": "Test Company",
  "url": "https://example.com",
};
const companyLogoObject = {
  url: "https://example.com/12345.png",
  width: 200,
  height: 50
};

// Stubs
const readFileStub = sinon.stub();

const AmpUtils = proxyquire('../src/amp-utils', {
  'fs': {
    'readFile': readFileStub
  }
});
const ampUtils = new AmpUtils();

describe('AmpUtils', function() {
  afterEach(function() {
    readFileStub.reset();
  });

  it('generateAmpHtml() should return html', function() {
    readFileStub.callsArgWith(2, null, dummyHtmlContent);
    const result = ampUtils.generateAmpHtml(templateView)
    return result.should.eventually.equal(dummyHtmlContent);
  });

  it('generateAmpHtml() should return error if failed reading the template file', function() {
    readFileStub.onCall(0).throws(new Error("some error"));
    const result = ampUtils.generateAmpHtml(templateView);
    return result.should.be.eventually.rejectedWith('some error');
  });

  it('generateAmpHtmlTemplateView() should return html', function() {
    const result = ampUtils.generateAmpHtmlTemplateView(s3Key, inputJson, companyLogoObject);
    expect(result).to.deep.equal({
      "canonicalLink": s3Key,
      "jobTitle": inputJson.jobTitle,
      "datePosted": dateFormat(new Date(), "yyyy-mm-dd"),
      "datePostedLongFormat": dateFormat(new Date(), "d mmm yyyy"),
      "jobDescription": "*Some job description*",
      "howToApply": "Send email to someone",
      "showAdditionalDescription": false,
      "jobLocation": inputJson.location,
      "salary": inputJson.salary,
      "companyName": inputJson.companyName,
      "companyLogo": companyLogoObject,
      "employmentType": inputJson.employmentType,
      "workHours": inputJson.workHours,
      "responsibilities": inputJson.responsibilities,
      "skills": inputJson.skills,
      "educationRequirements": inputJson.educationRequirements,
      "experienceRequirements": inputJson.experienceRequirements,
      "qualifications": inputJson.qualifications,
      "incentiveCompensation": inputJson.incentiveCompensation,
      "jobBenefits": inputJson.jobBenefits,
      "url": inputJson.url
    });
  });
});

const chai = require('chai');
const expect = chai.expect;

// Constants
const dummyJson = {};

// Stubs
const HttpResponseGenerator = require('../src/http-response-generator');
const httpResponseGenerator = new HttpResponseGenerator();

describe('HttpResponseGenerator', function() {
  it('execute() should generate http response', function() {
    const result = httpResponseGenerator.execute(dummyJson);
    const expectedResponse = {
      statusCode: 200,
      headers: { "Access-Control-Allow-Origin" : "*" },
      body: JSON.stringify(dummyJson)
    };
    expect(result).to.deep.equal(expectedResponse);
  });
});

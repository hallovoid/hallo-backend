const chai = require('chai');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const Jimp = require('jimp');
const config = require('../config');

// Constants
const companyLogo = "data:image/;base64,/somedummybase64values";
const companyLogoMaxWidth = config.web.company_logo_max_width;

// Stubs
const probeSyncStub = sinon.stub();
const image = {
  resize: sinon.stub(),
  getBuffer: sinon.stub(),
};
const jimpReadStub = sinon.stub().returns(image);

const ImageProcessor = proxyquire('../src/image-processor', {
  'jimp': {
    'read': jimpReadStub
  },
  'probe-image-size': {
    'sync': probeSyncStub
  }
});
const imageProcessor = new ImageProcessor();

describe('ImageProcessor', function() {
  afterEach(function() {
    probeSyncStub.reset();
  });

  it('processCompanyLogo() should not resize the company logo if company logo width is less than the max width', function() {
    probeSyncStub.returns({ width: companyLogoMaxWidth });
    imageProcessor.processCompanyLogo(companyLogo);
    jimpReadStub.should.have.not.been.called;
  });

  it('processCompanyLogo() should resize the company logo if company logo width is more than the max width', function() {
    probeSyncStub.returns({ width: companyLogoMaxWidth + 1 });
    image.getBuffer.callsArgWith(1, null, "dummy value")

    const result = imageProcessor.processCompanyLogo(companyLogo)

    return result.then(function() {
      jimpReadStub.should.have.been.calledOnce;
      image.resize.should.have.been.calledOnce.calledWith(companyLogoMaxWidth, Jimp.AUTO);
      image.getBuffer.should.have.been.calledOnce.calledWith(Jimp.MIME_PNG);;
    })
  });
});

const chai = require('chai');
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const mute = require('mute');
var config = require('../config');

// Constants
const domainName = config.web.domain;
const dummyResponse = { "dummyKey": "dummy value" }
const inputJson = {
  companyLogo: "dummy binary",
  email: "hello@hallo.io"
};
const companyLogoObject = {};
const generatedAmpUrl = "dummy url";

// Stubs
const ampHandlerExecuteStub = sinon.stub();
const companyLogoHandlerExecuteStub = sinon.stub().returns(companyLogoObject);
const paymentHandlerPayStub = sinon.stub();
const emailHandlerSendJobCreationEmailStub = sinon.stub();
const httpResponseGeneratorExecuteStub = sinon.stub().returns(dummyResponse);
const callback = sinon.stub();

const Job = proxyquire('../src/job', {
  './amp-handler': function() {
    return {
      'execute': ampHandlerExecuteStub
    };
  },
  './company-logo-handler': function() {
    return {
      'execute': companyLogoHandlerExecuteStub
    };
  },
  './payment-handler': function() {
    return {
      'pay': paymentHandlerPayStub
    };
  },
  './email-handler': function() {
    return {
      'sendJobCreationEmail': emailHandlerSendJobCreationEmailStub
    };
  },
  './http-response-generator': function() {
    return {
      'execute': httpResponseGeneratorExecuteStub
    };
  }
});
const job = new Job();

describe('Job', function() {
  beforeEach(function() {
    config.stripe.enabled = true;
  });

  afterEach(function() {
    ampHandlerExecuteStub.reset();
    companyLogoHandlerExecuteStub.reset();
    paymentHandlerPayStub.reset();
    emailHandlerSendJobCreationEmailStub.reset();
    httpResponseGeneratorExecuteStub.reset();
    callback.reset();
  });

  it('execute() should generate AMP, upload to S3 and then send email to the user', function() {
    ampHandlerExecuteStub.returns(generatedAmpUrl);
    console.log(config.stripe.enabled);
    const result = job.create(inputJson, callback);
    return result.then(function() {
      companyLogoHandlerExecuteStub.should.have.been.calledOnce.calledWith(domainName, inputJson.companyLogo);
      ampHandlerExecuteStub.should.have.been.calledOnce.calledWith(inputJson, domainName, companyLogoObject);
      paymentHandlerPayStub.should.have.been.calledOnce;
      emailHandlerSendJobCreationEmailStub.should.have.been.calledOnce.calledWith(inputJson.email, generatedAmpUrl);
      httpResponseGeneratorExecuteStub.should.have.been.calledOnce.calledWith({success: true, url: "dummy url"});
      callback.should.have.been.calledOnce.calledWith(null, dummyResponse);
    });
  });

  it('execute() should catch the error if error occurs', function() {
    const unmute = mute(); // Mute the error due to "throws"
    ampHandlerExecuteStub.throws();
    const result = job.create(inputJson, callback);
    return result.then(function() {
      companyLogoHandlerExecuteStub.should.have.been.calledOnce.calledWith(domainName, inputJson.companyLogo);
      ampHandlerExecuteStub.should.have.been.calledOnce.calledWith(inputJson, domainName, companyLogoObject);
      paymentHandlerPayStub.should.have.not.been.called;
      httpResponseGeneratorExecuteStub.should.have.been.calledOnce.calledWith({success: false});
      callback.should.have.been.calledOnce.calledWith(null, dummyResponse);
      unmute(); // Unmute to allow printing on stdout.
    });
  });

  it('execute() should not call paymentHandler if stripe is not enabled in config.js', function() {
    config.stripe.enabled = false;
    ampHandlerExecuteStub.returns(generatedAmpUrl);
    const result = job.create(inputJson, callback);
    return result.then(function() {
      companyLogoHandlerExecuteStub.should.have.been.calledOnce.calledWith(domainName, inputJson.companyLogo);
      ampHandlerExecuteStub.should.have.been.calledOnce.calledWith(inputJson, domainName, companyLogoObject);
      paymentHandlerPayStub.should.have.not.been.called;
      emailHandlerSendJobCreationEmailStub.should.have.been.calledOnce.calledWith(inputJson.email, generatedAmpUrl);
      httpResponseGeneratorExecuteStub.should.have.been.calledOnce.calledWith({success: true, url: "dummy url"});
      callback.should.have.been.calledOnce.calledWith(null, dummyResponse);
    });
  });
});

const chai = require('chai');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

// Constants
const domainName = "https://example.com";
const companyLogoBase64 = "data:image/;base64,/somedummybase64values";

// Stubs
const processCompanyLogoStub = sinon.stub().returns(companyLogoBase64);;
const uploadCompanyLogoS3Stub = sinon.stub();

const CompanyLogoHandler = proxyquire('../src/company-logo-handler', {
  './s3-utils': function() {
    return {
      'uploadCompanyLogoS3': uploadCompanyLogoS3Stub
    };
  },
  './image-processor': function() {
    return {
      'processCompanyLogo': processCompanyLogoStub
    };
  }
});
const companyLogoHandler = new CompanyLogoHandler();

describe('CompanyLogoHandler', function() {
  it('execute() should return null if company logo is not available', function() {
    const result = companyLogoHandler.execute(domainName, null);
    return result.then(function() {
      processCompanyLogoStub.should.have.not.been.called;
      uploadCompanyLogoS3Stub.should.have.not.been.called;
    });
  });

  it('execute() should process the company logo and upload the processed company logo to S3', function() {
    const result = companyLogoHandler.execute(domainName, companyLogoBase64);
    return result.then(function() {
      processCompanyLogoStub.should.have.been.calledOnce.calledWith(companyLogoBase64);
      uploadCompanyLogoS3Stub.should.have.been.calledOnce.calledWith(domainName, companyLogoBase64);
    });
  });
});

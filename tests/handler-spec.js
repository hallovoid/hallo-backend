const chai = require('chai');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

// Constants
const dummyJson = { "dummyKey": "dummy value" }
const dummyEvent = { "body": JSON.stringify(dummyJson) };
const dummyContext = {};
const callback = sinon.stub();

// Stubs
const jobCreateStub = sinon.stub();

const handler = proxyquire('../handler', {
  './src/job': function() {
    return {
      'create': jobCreateStub
    };
  }
});

describe('handler (entry point)', function() {
  it('create() should call job object to creat job posting', function() {
    handler.create(dummyEvent, dummyContext, callback);
    jobCreateStub.should.have.been.calledOnce.calledWith(dummyJson, callback);
  });

  it('hello() should return a hardcoded response', function() {
    handler.hello(dummyEvent, dummyContext, callback);
    callback.should.have.been.calledOnce;
  });
});

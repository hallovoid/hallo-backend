const chai = require('chai');
const expect = chai.expect;
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const Promise = require('bluebird');

// Constants
const domain = "https://somedomain.com";
const companyLogoS3Key = "somepath/somerandomkey.png";
const todayDate = new Date("December 23, 2016, 5:46:21 PM");
const inputJson = {
  companyName: "Hallo",
  jobTitle: "Software Engineer",
};
const s3Key = "20161223/hallo-software-engineer.html";
const ampHtmlContent = "assume there are some html tags here";
const ampHtml = new Buffer("some dummy string");
const imgBuffer = new Buffer("some dummy string");
const companyLogoWidth = 200;

// Stubs
const fakeS3Promise = function() { return Promise.resolve(); };
const putObjectStub = sinon.stub().returns({ promise: fakeS3Promise });
const probeSyncStub = sinon.stub().returns({ width: companyLogoWidth, height: 50 });

const S3Utils = proxyquire('../src/s3-utils', {
  'aws-sdk': {
    'S3': function () {
      return {
        putObject: putObjectStub
      };
    }
  },
  'probe-image-size': {
    'sync': probeSyncStub
  }
});
const s3Utils = new S3Utils();

describe('S3Utils', function() {
  afterEach(function() {
    putObjectStub.reset();
  });

  it('generateCompanyLogoS3Key() should return a short id with png extension', function() {
    expect(s3Utils.generateCompanyLogoS3Key()).to.match(/^img\/[\w]{7,12}.png$/);
  });

  it('generateCompanyLogoUrl() should return fully qualified url to the company logo', function() {
    expect(s3Utils.generateCompanyLogoUrl(domain, companyLogoS3Key)).to.equal(`${domain}/${companyLogoS3Key}`);
  });

  it('generateAmpHtmlS3Key() should return an S3 key for the generated AMP page', function() {
    expect(s3Utils.generateAmpHtmlS3Key(inputJson, todayDate)).to.equal("20161223/174621-hallo-software-engineer.html");
  });

  it('generateS3Params() should return S3 params for the html content', function() {
    const expectedOutput = {
      Bucket: "amp-job-pages",
      Key: s3Key,
      ContentType: 'text/html',
      Body: ampHtmlContent
    };
    expect(s3Utils.generateS3Params(s3Key, 'text/html', ampHtmlContent)).to.deep.equal(expectedOutput);
  });

  it('generateAmpHtmlUrl() should return fully qualified url to the AMP page', function() {
    expect(s3Utils.generateAmpHtmlUrl(domain, s3Key)).to.equal(`${domain}/${s3Key}`);
  });

  it('uploadCompanyLogoS3() should return a company logo object after image is uploaded to s3', function() {
    const result = s3Utils.uploadCompanyLogoS3(companyLogoS3Key, imgBuffer);
    return result.then(function(companyLogoObject) {
      putObjectStub.should.have.been.calledOnce;
      companyLogoObject.width.should.be.equal(companyLogoWidth);
    });
  });

  it('uploadAmpHtmlToS3() should be resolve promise after AMP is uploaded to s3', function() {
    const result = s3Utils.uploadAmpHtmlToS3(s3Key, ampHtml);
    return result.then(function() {
      putObjectStub.should.have.been.calledOnce;
    });
  });
});

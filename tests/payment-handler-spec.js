const chai = require('chai');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

// Constants
const dummyReturnedChargeObject = {
  "paid": true
};

// Stubs
const chargesCreateStub = sinon.stub().callsArgWith(1, null, dummyReturnedChargeObject);
const stripeStub = sinon.stub().returns({
  'charges': {
   'create': chargesCreateStub
  }
});


const PaymentHandler = proxyquire('../src/payment-handler', {
  'stripe': stripeStub
});
const paymentHandler = new PaymentHandler();

describe('PaymentHandler', function() {
  afterEach(function() {
    chargesCreateStub.reset();
  });

  it('pay() should make request to stripe for making payment', function() {
    const paymentObject = {};
    const result = paymentHandler.pay(paymentObject);
    return result.then(function(charge) {
      chargesCreateStub.should.have.been.calledOnce.calledWith(paymentObject);
      charge.paid.should.equal(true);
    });
  });
});
